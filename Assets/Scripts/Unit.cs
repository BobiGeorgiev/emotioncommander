﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Unit : MonoBehaviour
{
    public UnitStatReducable health;

    public UnitStat AttackDamage;
    public UnitStat Range;
    public UnitStat MovementPoints;
    public UnitStat Defence;
    public UnitStat Vision;
    public UnitStat Fatigue;
    public UnitStat MentalStrain;

    public string enemyTag;

    public GameObject healthBar;
    public GameObject CorpsePrefab;

    private bool hasFinishedMoving;

    [SerializeField]
    public UnitMentality mentality;
    public Inventory inventory;
    public bool HasFinishedTurn { get; set; }
    public bool HasFInishedMoving
    {
        get { Debug.Log("Wir sind hier"  +hasFinishedMoving); return hasFinishedMoving; }
        set {
            //if (mentality.CurrentEmState != null)
            //{
            //    if (!(mentality.CurrentEmState is Hopelessness) && value == false)
            //        return;
            //}
            hasFinishedMoving = value;
        }
    }

    public bool IsSleeping { get; set; }
    private int sleepCounter;

    public bool isReceivingFatigue;

    void Start()
    {
        HasFinishedTurn = false;

        health = new UnitStatReducable();
        health.maxValue = 5;
        health.currentValue = 5;
        healthBar.GetComponent<TextMeshPro>().text = health.currentValue.ToString();

        AttackDamage = new UnitStat();
        Range = new UnitStat();
        MovementPoints = new UnitStat();
        Defence = new UnitStat();
        Vision = new UnitStat();
        Fatigue = new UnitStat();
        MentalStrain = new UnitStat();

        //this is temporary since we want to load from a file
        AttackDamage.SetBaseValue(3);
        MovementPoints.SetBaseValue(3);
        Range.SetBaseValue(3);
        Defence.SetBaseValue(0); // this base is always 0 for now, maybe if body armor is introduced
        Vision.SetBaseValue(4);
        Fatigue.SetBaseValue(0);
        MentalStrain.SetBaseValue(0);


        isReceivingFatigue = true;

        IsSleeping = false;
        sleepCounter = 0;
    }

    public void DecreaseHealth(Unit n)
    {
        int temp = health.currentValue;
        health.currentValue = (health.currentValue + Defence.GetValue()) - n.AttackDamage.GetValue();

        mentality.IncreaseEmotion(new EmotionalEffect(Emotion.FEAR, 10 * health.currentValue - temp, "Injured"));
        MentalStrain.Increase(20);
        DisplayUnitStats();
    }

    public void DisplayUnitStats()
    {
        healthBar.GetComponent<TextMeshPro>().text = health.currentValue.ToString();
    }

    public void IncreaseFatigue(int f)
    {
        if (!isReceivingFatigue)
        {
            return;
        }
        int newFat = Fatigue.GetEffect() + f;

        if (Fatigue.GetValue() < 25 && newFat >= 25)
        {
            MentalStrain.Increase(1);
        }
        if (Fatigue.GetValue() < 50 && newFat >= 50 || Fatigue.GetValue() < 75 && newFat >= 75)
        {
            MentalStrain.Increase(2);
        }
        Fatigue.Increase(f);
    }

    public void ApplyTileEffects(TerrainType t)
    {
        Defence.SetTemporaryEffect(t.defenceBonus);
    }

    public void RemoveTileEffects(TerrainType t)
    {
        Defence.Decrease(t.defenceBonus);
    }

    public void ResetEffect()
    {
        AttackDamage.ResetTemporaryEffect();
        Range.ResetTemporaryEffect();
        MovementPoints.ResetTemporaryEffect();
        Defence.ResetTemporaryEffect();
    }
    public void TurnEnd()
    {
        if (IsSleeping)
        {
            Fatigue.Decrease(5);
            sleepCounter--;
            if(sleepCounter == 0)
                IsSleeping = false;
            return;
        }

        IncreaseFatigue(1);
        mentality.DecreaseEmotionalStateCounter(1);
        mentality.ClearCurrentCauses();
    }

    public void ApplyEmotionalEffect(List<EmotionalEffect> effects)
    {
        mentality.ApplyPositionEffects(effects);
    }

    public void MentalEffectChance()
    {
        mentality.EmotionalStateChance();
    }

    public bool CanPerformActions()
    {
        if (HasFinishedTurn == true)
            return false;
        if (IsSleeping == true)
            return false;
        return true;
    }

    public void Sleep()
    {
        IsSleeping = true;
        sleepCounter = 3; //increase with one more than you want
    }

    public void IncreaseEmotion(EmotionalEffect em)
    {
        mentality.IncreaseEmotion(em);
    }

    public void GetCurrentMentality()
    {
        mentality.GetCurrentMentality();
    }
}
