﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewTerrainType", menuName = "TerrainType")]
public class TerrainType : ScriptableObject
{
    public int defenceBonus;
    public int movementCost;
    public string description;

    public Sprite visual;

}