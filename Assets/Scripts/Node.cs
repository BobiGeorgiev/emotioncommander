﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    public int posX;
    public int posY;

    private bool selected;
    private bool selectable;
    private bool hasEnemyInRange;

    Color defaultColor = new Color(.9f, .9f, .9f, .7f);
    Color selectedColor = new Color(1f, 1f, 1f, 1f);
    Color selectableColor = new Color(.5f, 1f, .5f, 1f);
    Color attackableColor = new Color(1f, .5f, .5f, 1f);

    public List<Node> neighbours;

    public TerrainType tileOnNode;
    public GameObject unitOnNode;

    //may include corpses, items
    public List<GameObject> objectsOnNode;

    public GameObject backgroundPrefab;
    SpriteRenderer spriteRenderer;

    TileMap map;

    //BFS (Breadth First Search)
    public bool visited = false;
    public Node parent = null;
    public int distance = 0;

    public void SetBackGroundPrefab()
    {
        backgroundPrefab.GetComponent<SpriteRenderer>().sprite = tileOnNode.visual;
        spriteRenderer = backgroundPrefab.GetComponent<SpriteRenderer>();
        spriteRenderer.color = defaultColor;
    }

    public void Reset()
    {
        ResetBFS();

        Selectable = false;
        HasEnemyInRange = false;
    }

    public void Deselectable()
    {
        ResetBFS();

        Selectable = false;
    }

    public void ResetBFS()
    {
        visited = false;
        parent = null;
        distance = 0;
    }

    public bool Selected
    {
        get { return selected; }
        set
        {
            if (value)
            {
                if(unitOnNode!= null)
                {
                    unitOnNode.GetComponent<Unit>().GetCurrentMentality();
                }
                spriteRenderer.color = selectedColor;
            }
            else
            {
                Debug.Log("What the fuck");
                spriteRenderer.color = defaultColor;
            }
            selected = value;
        }
    }

    public bool Selectable
    {
        get { return selectable; }
        set {
            if (value)
            {
                spriteRenderer.color = selectableColor;
            }
            else
            {
                spriteRenderer.color = defaultColor;
            }
            selectable = value;
        }
    }

    public bool HasEnemyInRange
    {
        get { return hasEnemyInRange; }
        set {
            if (value)
            {
                spriteRenderer.color = attackableColor;
            }
            else
            {
                spriteRenderer.color = defaultColor;
            }
            hasEnemyInRange = value;
        }
    }

    void Awake()
    {
        neighbours = new List<Node>();
    }

    void Start()
    {
        map = GameObject.FindGameObjectWithTag("Map").GetComponent<TileMap>();
    }

    void OnMouseOver()
    {
        //left mouse button
        if (Input.GetMouseButtonDown(0))
        {
            if (objectsOnNode.Count > 0)
            {
                Debug.Log("RIIIIZE");
            }
            map.TileClick(this, 0);
        }
        //right mouse button
        else if (Input.GetMouseButtonDown(1))
        {
            map.TileClick(this, 1);
        }
    }

    public override string ToString()
    {
        return "Loc: " + posX + "," + posY + " select:" + selectable + " visit:" + visited + " parent:" + parent + " dist:" + distance + " neigh" + neighbours.Count;
    }

}
