﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmotionalEffect 
{

    public Emotion state;
    public int amount;
    public string cause;
    public EmotionalEffect(Emotion _state, int _amount, string _cause)
    {
        state = _state;
        amount = _amount;
        cause = _cause;
    }
}
