﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recklessness : EmotionalState
{
    public void ApplyEffects(Unit u)
    {
        u.isReceivingFatigue = false;
        u.MovementPoints.Increase(2);

        Debug.Log("Recklessness effects applied");
    }

    public string GetEffectsDescription()
    {
        return "Fatigue ignored \n Movement + 2";
    }

    public string GetTitle()
    {
        return "Hopelessness";
    }

    public void RemoveEffects(Unit u)
    {
        u.isReceivingFatigue = true;
        u.MovementPoints.Decrease(2);

        Debug.Log("Recklessness effects removed");
    }
}
