﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guilt : EmotionalState
{
    public void ApplyEffects(Unit u)
    {
        u.MovementPoints.Increase(2);
        u.AttackDamage.Decrease(1);
    }

    public string GetEffectsDescription()
    {
        return "Movement +1 \n Attack Damage -1"; 
    }

    public string GetTitle()
    {
        return "Guilt";
    }

    public void RemoveEffects(Unit u)
    {
        u.MovementPoints.Decrease(2);
        u.AttackDamage.Increase(1);
    }
}
