﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hopelessness : EmotionalState
{
    public void ApplyEffects(Unit u)
    {
        u.Range.Increase(2);
        u.HasFInishedMoving = true;
    }

    public string GetEffectsDescription()
    {
        return "Range +2 \n Cannot move";
    }

    public string GetTitle()
    {
        return "Hopelessness";
    }

    public void RemoveEffects(Unit u)
    {
        u.Range.Decrease(2);
    }
}
