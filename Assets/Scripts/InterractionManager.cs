﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterractionManager : MonoBehaviour
{
    public List<Node> canHeal;
    public List<Node> enemiesInRange;
    public List<Node> canGiveItem;

    [SerializeField]
    TileMap tileMap;

    void Start()
    {
        canHeal = new List<Node>();
        enemiesInRange = new List<Node>();
        canGiveItem = new List<Node>();
    }

    public List<Interraction> GetInterraction(Unit u, Node pos)
    {
        canHeal.Clear();
        enemiesInRange.Clear();
        canGiveItem.Clear();

        List<Interraction> temp = new List<Interraction>();

        temp.Add(Interraction.SLEEP);
        if (u.inventory.Vodka.HasAny())
        {
            temp.Add(Interraction.VODKA);
            canGiveItem.Add(pos);
        }
        if (u.inventory.Medkit.HasAny() && !u.health.IsMax()){
            canHeal.Add(pos);
            temp.Add(Interraction.HEAL);
        }

        ThingsAdjacent(pos, u);
        if (canHeal.Count > 0)
            temp.Add(Interraction.HEAL);
        if (enemiesInRange.Count > 0)
            temp.Add(Interraction.MELEE);

        return temp;
    }

    void ThingsAdjacent(Node n, Unit u)
    {
        try
        {
            Node look = tileMap.nodes[n.posX - 1, n.posY];
            CheckNode(look, u);
        }
        catch (System.IndexOutOfRangeException) { }
        try
        {
            Node look = tileMap.nodes[n.posX + 1, n.posY];
            CheckNode(look, u);
        }
        catch (System.IndexOutOfRangeException) { }
        try
        {
            Node look = tileMap.nodes[n.posX, n.posY - 1];
            CheckNode(look, u);
        }
        catch (System.IndexOutOfRangeException) { }
        try
        {
            Node look = tileMap.nodes[n.posX, n.posY + 1];
            CheckNode(look, u);
        }
        catch (System.IndexOutOfRangeException) { }
    }

    void CheckNode(Node look, Unit u)
    {
        if (look.unitOnNode != null)
        {
            Unit lookUnit = look.unitOnNode.GetComponent<Unit>();
            if (lookUnit.tag == u.tag)
            {
                Debug.Log("Fri " + look.posX + " " + look.posY);
                canGiveItem.Add(look);
                if(!lookUnit.health.IsMax() && u.inventory.Medkit.HasAny())
                    canHeal.Add(look);
            }
            if (lookUnit.tag == u.enemyTag)
            {
                enemiesInRange.Add(look);
            }
        }
    }
}
