﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corpse : MonoBehaviour
{
    public string UnitTag;
    public string UnitName;
    void Start()
    {
        
    }

    public void GiveIdentity(Unit u)
    {
        UnitTag = u.tag;
        UnitName = u.name;
    }
}
