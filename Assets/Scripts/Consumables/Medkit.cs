﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medkit : Consumable
{

    public override void ApplyEffects(Unit u)
    {
        u.health.Increase(3);
        u.Fatigue.Decrease(20);
        u.MentalStrain.Decrease(3);
        u.DisplayUnitStats();
        amount--;
    }
}
