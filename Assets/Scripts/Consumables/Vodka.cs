﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vodka : Consumable
{
    public override void ApplyEffects(Unit u)
    {
        u.Fatigue.Decrease(5);
        //u.MentalStrain.Decrease(5);
        u.IncreaseEmotion(new EmotionalEffect(Emotion.COURAGE, 20, "Real alcohol for strong persons"));
    }
}
