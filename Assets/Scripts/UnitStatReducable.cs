﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStatReducable 
{

    public int maxValue;

    public int currentValue;

    public void Decreace(int amount)
    {
        currentValue -= amount;
    }

    public void Increase(int amount)
    {
        currentValue += amount;
        if (currentValue > maxValue)
            currentValue = maxValue;
    }

    public bool IsMax()
    {
        if (maxValue == currentValue)
            return true;
        return false;
    }

    public string GetValueString()
    {
        return currentValue + "/" + maxValue;
    }
}
