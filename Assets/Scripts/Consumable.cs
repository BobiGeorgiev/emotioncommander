﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consumable 
{
    protected int amount;

    public void Collect(int amount)
    {
        this.amount += amount;
    }

    public int Amount()
    {
        return amount;
    }

    public bool HasAny()
    {
        if (amount > 0)
            return true;
        return false;
    }

    public virtual void ApplyEffects(Unit u)
    {

    }
}
