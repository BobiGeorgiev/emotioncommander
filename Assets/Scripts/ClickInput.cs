﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ClickInput
{
   RightMouseClick,
   SameTileClick,
   EmptyTileClick,
   FriendlyClick,
   EnemyClick
}
