﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitMentality : MonoBehaviour
{
    private UnitStat guilt;
    private UnitStat courage;
    private UnitStat fear;

    [SerializeField]
    private Unit thisUnit;

    List<EmotionalEffect> currentPositionEffectCauses;
    List<EmotionalEffect> currentCauses;

    EmotionalState currentEmotionalState;
    int emotionalStateCounter;

    public EmotionalState CurrentEmState
    {
        get { return currentEmotionalState; }
    }

    public UnitStat Guilt
    {
        get { return guilt; }
    }
    public UnitStat Courage
    {
        get { return courage; }
    }
    public UnitStat Fear
    {
        get { return fear; }
    }
    void Awake()
    {
        currentPositionEffectCauses = new List<EmotionalEffect>();
        currentCauses = new List<EmotionalEffect>();
        guilt = new UnitStat();
        courage = new UnitStat();
        fear = new UnitStat();

        guilt.SetBaseValue(0);
        courage.SetBaseValue(0);
        fear.SetBaseValue(0);

        emotionalStateCounter = 0;
    }

    public void IncreaseEmotion(EmotionalEffect em)
    {
        switch (em.state)
        {
            case Emotion.GUILT:
                guilt.Increase(em.amount);
                break;
            case Emotion.FEAR:
                fear.Increase(em.amount);
                break;
            case Emotion.COURAGE:
                courage.Increase(em.amount);
                break;
        }
        currentCauses.Add(em);
    }

    public void ApplyPositionEffects(List<EmotionalEffect> effects)
    {
        currentPositionEffectCauses.Clear();

        foreach(EmotionalEffect m in effects)
        {
            switch (m.state)
            {
                case Emotion.GUILT:
                    guilt.Increase(m.amount);
                    break;
                case Emotion.COURAGE:
                    courage.Increase(m.amount);
                    break;
                case Emotion.FEAR:
                    fear.Increase(m.amount);
                    break;
            }
            currentPositionEffectCauses.Add(m);

            thisUnit.MentalStrain.Increase(1);
            Debug.Log("Emotion " + m.state + " Reason: " + m.cause);
        }
        Debug.Log("Fear" + fear.GetValue());
    }

    public void EmotionalStateChance()
    {
        if (currentEmotionalState != null)
            return;

        if(thisUnit.MentalStrain.GetValue() < 15)
        {
            return;
        }
        int chance = Random.Range(0, 100);
        Debug.Log("Chance " + chance);
        if (chance < thisUnit.MentalStrain.GetValue() * 2)
        {
            if(courage.GetValue() > fear.GetValue() && courage.GetValue() > guilt.GetValue())
             EnterEmotionState(Emotion.COURAGE);
            if (fear.GetValue() > courage.GetValue() && fear.GetValue() > guilt.GetValue())
                EnterEmotionState(Emotion.FEAR);
            if (guilt.GetValue() > courage.GetValue() && guilt.GetValue() > fear.GetValue())
                EnterEmotionState(Emotion.GUILT);
        }
    }

    void EnterEmotionState(Emotion em)
    {
        switch (em)
        {
            case Emotion.GUILT:
                break;
            case Emotion.COURAGE:
                currentEmotionalState = new Recklessness();
                courage.IncreasePermanently(20);
                break;
            case Emotion.FEAR:
                currentEmotionalState = new Hopelessness();
                fear.IncreasePermanently(20);
                Debug.Log("I dont even know");
                break;
        }
        currentEmotionalState.ApplyEffects(thisUnit);
        emotionalStateCounter = 3;
    }

    public void DecreaseEmotionalStateCounter(int amount)
    {
        if(currentEmotionalState == null)
            return;
        Debug.Log("Made it here");
        emotionalStateCounter -= amount;
        if(emotionalStateCounter <= 0)
        {
            currentEmotionalState.RemoveEffects(thisUnit);
            thisUnit.MentalStrain.SetTemporaryEffect(0);
            emotionalStateCounter = 0;
            currentEmotionalState = null;
        }
    }

    public void ClearCurrentCauses()
    {
        currentCauses.Clear();
    }

    public List<EmotionalEffect> GetCurrentMentality()
    {
        List<EmotionalEffect> temp = new List<EmotionalEffect>();
        foreach(EmotionalEffect m in currentPositionEffectCauses)
        {
            temp.Add(m);
        }
        foreach(EmotionalEffect m in currentCauses)
        {
            temp.Add(m);
        }
        return temp;
    }
}