﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainTile : MonoBehaviour
{
    public TerrainType terrain;

    public int DefenceBonus
    {
        get { return terrain.defenceBonus; } 
    }

    public int MovementCost
    {
        get { return terrain.movementCost; }
    }

    public Sprite TileVisual()
    {
        if (terrain.visual != null)
        {
            return terrain.visual;
        }
        else
        {
            return null;
        }
    }
}
