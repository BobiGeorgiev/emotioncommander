﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface EmotionalState
{
     void ApplyEffects(Unit u);

    void RemoveEffects(Unit u);

    string GetTitle();

    string GetEffectsDescription();
}
