﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStat 
{
    private int statBase;
    private int statAdded;

    public void Increase(int s)
    {
        statAdded += s;
    }

    public void Decrease(int s)
    {
        statAdded -= s;
        if(statAdded < 0)
            statAdded = 0;
    }

    public int GetValue()
    {
        return statBase + statAdded;
    } 

    public int GetEffect()
    {
        return statAdded;
    }

    public bool HasValueAdded()
    {
        if (statAdded != 0)
            return true;
        return false;
    }
    public void SetTemporaryEffect(int s)
    {
        statAdded = s;
    }

    public void ResetTemporaryEffect()
    {
        statAdded = 0;
    }

    public void IncreasePermanently(int s)
    {
        statBase += s;
    }

    public void SetBaseValue(int s)
    {
        statBase = s;
    }
}
