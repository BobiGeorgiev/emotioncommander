﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTurnManager 
{
    List<PlayerTurn> players;
    public PlayerTurn currentPlayer;
    int currentPlayerPosition;    
    public PlayerTurnManager()
    {
        currentPlayerPosition = 0;
        players = new List<PlayerTurn>();
    }

    public void AddNewPlayer(string tag)
    {
        players.Add(new PlayerTurn(tag));
        currentPlayer = players[currentPlayerPosition];
    }

    public void NextPlayer()
    {
        currentPlayerPosition += 1;
        if(players.Count == currentPlayerPosition)
        {
            currentPlayerPosition = 0;
        }
        currentPlayer = players[currentPlayerPosition];
    }
}
