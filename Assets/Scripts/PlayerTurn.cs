﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTurn 
{
    public string playerTag;
    public List<Unit> playerUnits;

    public PlayerTurn(string tag)
    {
        playerTag = tag;
        playerUnits = new List<Unit>();
    }
}
