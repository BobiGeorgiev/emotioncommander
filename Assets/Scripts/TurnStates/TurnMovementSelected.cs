﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMovementSelected : TurnState
{
    protected override void BackClick()
    {
        tileMap.DeselectInterraction();
        tileMap.NextTurnState(previousState);
    }

    protected override void EmptyClick()
    {
        if (tileMap.SelectMovementPosition())
        {
            tileMap.SetTileToInterract();
        }
    }

    protected override void SameClick()
    {
        //tileMap.MoveUnit();
        //tileMap.Clear();
        //tileMap.NextTurnState(new StateNonSelected(tileMap));

        tileMap.MoveUnit();
        tileMap.SelectNode();

      //  tileMap.ResetSelectableNodes();
        tileMap.NextTurnState(new TurnHasMoved());
    }

    protected override void EnemyClick()
    {
        if (tileMap.TargetEnemy())
        {
            tileMap.SetTileToInterract();
            tileMap.NextTurnState(new StateEnemyTargeted());
        }
    }
    protected override void FriendlyClick()
    {
        tileMap.SetTileToInterract();
        tileMap.SelectNode();
    }
}
