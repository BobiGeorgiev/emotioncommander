﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateNonSelected : TurnState
{
    public StateNonSelected(TileMap t)
    {
        tileMap = t;
    }

    protected override void BackClick()
    {
        tileMap.Clear();
    }

    protected override void EmptyClick()
    {
        tileMap.SetTileToInterract();
        tileMap.SelectNode();
    }

    protected override void FriendlyClick()
    {
        tileMap.SetTileToInterract();
        tileMap.SelectNode();
        tileMap.NextTurnState(new StateUnitSelected());
    }

    protected override void EnemyClick()
    {
        Debug.Log("Nani sore");
        tileMap.DisplayTileInfo();
        tileMap.DisplayUnitInfo();
    }
}
