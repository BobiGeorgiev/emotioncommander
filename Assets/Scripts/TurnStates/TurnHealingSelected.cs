﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnHealingSelected : TurnState
{
    protected override void BackClick()
    {
        tileMap.DeselectInterraction();
        tileMap.NextTurnState(previousState);
    }

    protected override void FriendlyClick()
    {
        tileMap.SetTileToInterract();
        if (tileMap.CanBeGivenConsumable())
        {
            Debug.Log("www");
            tileMap.ApplyItem();
            tileMap.Clear();
            tileMap.NextTurnState(new StateNonSelected(tileMap));
        }
    }

    protected override void SameClick()
    {
        if (tileMap.CanBeGivenConsumable())
        {
            Debug.Log("www");
            tileMap.ApplyItem();
            tileMap.Clear();
            tileMap.NextTurnState(new StateNonSelected(tileMap));
        }
    }
}
