﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateEnemyTargeted : TurnState
{
    protected override void BackClick()
    {
        Debug.Log("mmmm yas");
        tileMap.DeselectInterraction();
        tileMap.NextTurnState(previousState);
    }
    protected override void EnemyClick()
    {
        if (tileMap.SelectMovementPosition())
        {
            tileMap.SetTileToInterract();
        }
    }
    protected override void SameClick()
    {
        tileMap.AttackUnit();
        tileMap.Clear();
        tileMap.NextTurnState(new StateNonSelected(tileMap));
    }
  
}
