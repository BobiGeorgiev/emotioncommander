﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnMeleeSelected : TurnState
{
    protected override void BackClick()
    {
        tileMap.DeselectInterraction();
        tileMap.NextTurnState(previousState);
    }

    protected override void EnemyClick()
    {
        tileMap.SetTileToInterract();
        if (tileMap.CanBeMeleed())
        {
            tileMap.MeleeAttack();
            tileMap.Clear();
            tileMap.NextTurnState(new StateNonSelected(tileMap));
        }
    }
}
