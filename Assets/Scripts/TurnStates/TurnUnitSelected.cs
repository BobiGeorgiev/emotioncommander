﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateUnitSelected : TurnState
{
    protected override void BackClick()
    {
        tileMap.Clear();
        tileMap.NextTurnState(new StateNonSelected(tileMap));
    }

    protected override void EmptyClick()
    {
        if (tileMap.SelectMovementPosition())
        {
            tileMap.SetTileToInterract();
            tileMap.NextTurnState(new StateMovementSelected());
        }
    }

    protected override void EnemyClick()
    {
        Debug.Log("Hello mujahadeen");
        if (tileMap.TargetEnemy())
        {
            tileMap.SetTileToInterract();
            tileMap.NextTurnState(new StateEnemyTargeted());
        }
    }

    protected override void FriendlyClick()
    {
        tileMap.SetTileToInterract();
        tileMap.SelectNode();
    }
}
