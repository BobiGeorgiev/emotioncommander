﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnHasMoved : TurnState
{
    protected override void BackClick()
    {
        tileMap.Clear();
        tileMap.NextTurnState(new StateNonSelected(tileMap));
    }

    protected override void EnemyClick()
    {
        Debug.Log("Hello mujahadeen");
        if (tileMap.TargetEnemy())
        {
            tileMap.SetTileToInterract();
            tileMap.NextTurnState(new StateEnemyTargeted());
        }
    }
}
