﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{

    public Medkit Medkit;
    public Vodka Vodka;
    void Start()
    {
        Medkit = new Medkit();
        Medkit.Collect(2);

        Vodka = new Vodka();
        Vodka.Collect(2);
    }
}
