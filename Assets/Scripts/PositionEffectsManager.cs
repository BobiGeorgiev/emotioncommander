﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionEffectsManager 
{
    List<EmotionalEffect> currentPositionEffects;
    TileMap tileMap;
    List<Node> enemies;
    List<Node> corpseNodes;
    List<Corpse> corpsesObject;
    List<Node> friendlies;

    public bool FriendliesNearby;
    public bool OneEnemyNearby;

    public PositionEffectsManager(TileMap t)
    {
        tileMap = t;
        currentPositionEffects = new List<EmotionalEffect>();
        enemies = new List<Node>();
        corpseNodes = new List<Node>();
        corpsesObject = new List<Corpse>();
        friendlies = new List<Node>();

        FriendliesNearby = false;
        OneEnemyNearby = false;
    }

    public List<EmotionalEffect> GetEffectsAlreadyCalculated()
    {
        return currentPositionEffects;
    }

    public List<EmotionalEffect> GetEffectsOnPosition(Node newPos, Node currentPos)
    {
        Clear();

        Unit unitOnPos = currentPos.unitOnNode.GetComponent<Unit>();

        //Debug.Log("Get Effects");
        foreach(Node lookNode in tileMap.nodes)
        {
            if (lookNode == currentPos)
                continue;

            if(lookNode.unitOnNode!= null)
            {
                if (lookNode.unitOnNode.CompareTag(unitOnPos.enemyTag))
                {
                    enemies.Add(lookNode);
                }
                if (lookNode.unitOnNode.CompareTag(unitOnPos.tag))
                {
                    friendlies.Add(lookNode);
                }
            }
            if(lookNode.objectsOnNode.Count != 0)
            {
                foreach(GameObject obj in lookNode.objectsOnNode)
                {
                    if(obj.tag == "Corpse")
                    {
                        corpseNodes.Add(lookNode);
                        corpsesObject.Add(obj.GetComponent<Corpse>());
                        Debug.Log("Corpse party");
                    }
                }
            }
        }

        LondonExists();
        AllyCorpseNearby(newPos, unitOnPos);
        NoAlliesNearby(newPos, unitOnPos);
        EnemiesNearby(newPos, unitOnPos);
        TileEffects(unitOnPos, newPos);

        return currentPositionEffects;
    }

    void Clear()
    {
        currentPositionEffects.Clear();
        enemies.Clear();
        corpseNodes.Clear();
        friendlies.Clear();

        FriendliesNearby = false;
        OneEnemyNearby = false;
    }

    void AllyCorpseNearby(Node n, Unit u)
    {
        for(int i = 0; i < corpseNodes.Count; i++)
        {
            Debug.Log("Hipodil");
            if (tileMap.DistanceBetweenNodes(n, corpseNodes[i]) <= 2)
            {
                if(corpsesObject[i].UnitTag == u.tag)
                {
                    currentPositionEffects.Add(new EmotionalEffect(Emotion.GUILT, 10, "Ally corpse nearbly"));
                    u.MentalStrain.Increase(5);
                }
            }
        }
    }

    void EnemiesNearby(Node n, Unit u)
    {
        int count = 0;
        foreach(Node look in enemies)
        {
            Debug.Log("At least here");
            if (tileMap.DistanceBetweenNodes(n, look) <= u.Vision.GetValue())
            {
                Debug.Log("We made it tgis far");
                count++;
                OneEnemyNearby = true;
            }
        }
        if (count > 2)
        {
            currentPositionEffects.Add(new EmotionalEffect(Emotion.FEAR, 10, "Enemies closeby"));
            u.MentalStrain.Increase(10);
        }
    }

    void NoAlliesNearby(Node n, Unit u)
    {
        bool nearby = false;
        foreach (Node look in friendlies)
        {
            if(tileMap.DistanceBetweenNodes(n, look) <= u.Vision.GetValue())
            {
                nearby = true;
                break;
            }
        }
        if (!nearby)
        {
            currentPositionEffects.Add(new EmotionalEffect(Emotion.FEAR, 10, "No allies nearby"));
            u.MentalStrain.Increase(5);
        }
        FriendliesNearby = nearby;
    }

    void LondonExists()
    {
        //urrentPositionEffects.Add(new EmotionalEffect(Emotion.FEAR, 10, "London is a real place and people actually live there"));
    }

    void TileEffects(Unit u, Node n)
    {
        if(n.tileOnNode == tileMap.terrainTypes[2])
        {
            u.MentalStrain.Decrease(10);
        }
    }
}