﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMap : MonoBehaviour
{

    PlayerTurnManager playerTurn;
    PositionEffectsManager posEffectsManager;

    public int mapSizeX = 12;
    public int mapSizeY = 12;

    public GameObject tileBasePrefab;
    public TerrainType[] terrainTypes;
    //this game is set to only have two teams - player team and enemy team
    public GameObject allyPrefab;
    public GameObject enemyPrefab;

    public MapUI mapUI;
    [SerializeField]
    private InterractionManager interractionManager;

    public Node[,] nodes;

    //here start selection variables

    //when everything is deselected and a node is clicked on, it becomes the selectedNode
    public Node selectedNode;
    public GameObject selectedUnitGameObject;
    public Unit selectedUnit;
    //this is the node the selectedNode will interract with
    private Node nodeToInterractWith;
    private Node clickedNode;
    private Consumable selectedConsumable;
    //those are nodes the unit can interract with
    //an example is giving items to ally units, like medkits and vodka
    private List<Node> selectedInterractionNodes;

    public TurnState currentTurnState;

    private List<Node> selectableNodes;
    //possibly not needed
    private List<Node> enemiesInRange;


    // Debugging purposes, will be disabled in final build
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            EndPlayerTurn();
            Debug.Log(playerTurn.currentPlayer.playerTag);
            StartNewPlayerTurn(playerTurn.currentPlayer.playerTag);
            mapUI.SetPlayerTurn(playerTurn.currentPlayer.playerTag);
        }
        if (Input.GetKeyDown("z"))
        {
            Debug.Log("It just works");
            if (selectedNode.tileOnNode == terrainTypes[0])
            {
                Debug.Log("heiii");
            }
            Clear();
        }
    }

    void Start()
    {
        mapUI = transform.GetComponent<MapUI>();

        playerTurn = new PlayerTurnManager();
        playerTurn.AddNewPlayer("Player");
        playerTurn.AddNewPlayer("Enemy");

        posEffectsManager = new PositionEffectsManager(this);

        nodes = new Node[mapSizeX, mapSizeY];
        selectableNodes = new List<Node>();
        enemiesInRange = new List<Node>();
        selectedNode = null;
        selectedUnitGameObject = null;
        selectedUnit = null;

        currentTurnState = new StateNonSelected(this);

        GenerateNodes();
        DetermineTileTypes();
        GenerateTiles();
        FindNeighbourNodes();

        //all info on units will be done through a file in the future
        SpawnUnit(2,2);
        SpawnUnit(2,4);
        SpawnUnit(2, 5);
        SpawnUnit(2, 7);
        nodes[2, 7].unitOnNode.GetComponent<Unit>().mentality.Fear.Increase(40);
        nodes[2, 2].unitOnNode.GetComponent<Unit>().mentality.Courage.Increase(40);

        SpawnEnemy(11,4);
        SpawnEnemy(11, 5);
        SpawnEnemy(11, 7);
        SpawnEnemy(11, 8);
        nodes[11, 8].unitOnNode.GetComponent<Unit>().mentality.Courage.Increase(40);

        playerTurn.currentPlayer.playerUnits = GetThisPlayerUnits();
        mapUI.SetPlayerTurn(playerTurn.currentPlayer.playerTag);
    }

    //instantiate base node prefab without specifying what tile is has
    void GenerateNodes()
    {
        Debug.Log("Nani sore " + mapSizeX);
        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                GameObject baseTile = Instantiate(tileBasePrefab, new Vector2(x, y), Quaternion.identity);
                nodes[x, y] = baseTile.GetComponent<Node>();
                nodes[x, y].posX = x;
                nodes[x, y].posY = y;

            }
        }
    }

    //set what terrain each node will have
    void DetermineTileTypes()
    {
        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                nodes[x, y].tileOnNode = terrainTypes[0];
            }
        }

        //this will be done through a file
        nodes[2, 2].tileOnNode = terrainTypes[1];

        nodes[6, 5].tileOnNode = terrainTypes[1];
        nodes[6, 6].tileOnNode = terrainTypes[1];
        nodes[6, 7].tileOnNode = terrainTypes[1];
        nodes[7, 6].tileOnNode = terrainTypes[1];

        nodes[5, 5].tileOnNode = terrainTypes[1];
        nodes[5, 6].tileOnNode = terrainTypes[1];
        nodes[5, 7].tileOnNode = terrainTypes[1];
        nodes[5, 8].tileOnNode = terrainTypes[1];


        nodes[9, 5].tileOnNode = terrainTypes[1];
        nodes[7, 4].tileOnNode = terrainTypes[1];
        nodes[8, 8].tileOnNode = terrainTypes[1];
        nodes[8, 3].tileOnNode = terrainTypes[1];

        nodes[5, 2].tileOnNode = terrainTypes[2];
        nodes[5, 3].tileOnNode = terrainTypes[2];
        nodes[4, 5].tileOnNode = terrainTypes[2];
        nodes[4, 6].tileOnNode = terrainTypes[2];

        nodes[7, 5].tileOnNode = terrainTypes[2];
        nodes[7, 6].tileOnNode = terrainTypes[2];
        nodes[8, 9].tileOnNode = terrainTypes[2];
        nodes[8, 1].tileOnNode = terrainTypes[2];

    }

    //assign tiles to nodes as set in DetermineTileTypes()
    void GenerateTiles()
    {
        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                Sprite v = nodes[x, y].tileOnNode.visual;
                if(v != null)
                {
                    // nodes[x, y].backgroundPrefab.GetComponent<SpriteRenderer>().sprite = v;
                    nodes[x, y].SetBackGroundPrefab();
                }
                else
                {
                    Debug.Log("No visual on: " + x + " " + y + "!");
                }
            }
        }
    }

    //changing between turn states
    //ususally called by the current turn state
    //also called by methods interracting with other units
    public void NextTurnState(TurnState nex)
    {
        TurnState prev = currentTurnState;
        currentTurnState = nex;
        currentTurnState.previousState = prev;
        currentTurnState.tileMap = this;
    }

    void SpawnUnit(int x, int y)
    {
        GameObject allyUnit = Instantiate(allyPrefab) as GameObject;
        PlaceUnit(x, y, allyUnit);
    }

    // spawns new enemy unit 
    void SpawnEnemy(int x, int y)
    {
        GameObject enemyUnit = Instantiate(enemyPrefab) as GameObject;
        PlaceUnit(x, y, enemyUnit);
    }

    //changes the physical location of a game object
    //Assigns unit to the node
    void PlaceUnit(int x, int y, GameObject u)
    {
        u.transform.position = new Vector2(x, y + 0.1f);
        nodes[x, y].unitOnNode = u;
    }

    //whenever a new click is received, this method determines what ClickInput
    //it then sends it to the currentTurnState
    public void TileClick(Node clNode, int mouseClick)
    {
        clickedNode = clNode;

        if(selectedNode != null)
        {
            //Debug.Log("Gibe distance" + DistanceBetweenNodes(selectedNode, clNode));
        }

        switch (mouseClick)
        {
            case 1:
                currentTurnState.GetInput(ClickInput.RightMouseClick);
                break;
            case 0:
                if (nodeToInterractWith != null)
                {
                    if (nodeToInterractWith == clickedNode)
                    {
                        currentTurnState.GetInput(ClickInput.SameTileClick);
                        return;
                    }
                }
                if (clickedNode.unitOnNode != null && selectedUnit != null)
                {
                    if (clickedNode.unitOnNode.CompareTag(selectedUnitGameObject.tag))
                    {
                        currentTurnState.GetInput(ClickInput.FriendlyClick);
                        return;
                    }
                    else if (clickedNode.unitOnNode.CompareTag(selectedUnit.enemyTag))
                    {
                        currentTurnState.GetInput(ClickInput.EnemyClick);
                        return;
                    }
                }
                else if(clickedNode.unitOnNode != null)
                {
                    if (clickedNode.unitOnNode.CompareTag(playerTurn.currentPlayer.playerTag))
                    {
                        currentTurnState.GetInput(ClickInput.FriendlyClick);
                        return;
                    }
                }
                if(clickedNode.unitOnNode == null)
                {
                    currentTurnState.GetInput(ClickInput.EmptyTileClick);
                }
                break;
        }
    }

    //setting the clicked node to interract
    public void SetTileToInterract()
    {
        nodeToInterractWith = clickedNode;
    }


    //Clicking on a node that is empty/has enemy will provide info for it
    //Clicking on a node with friendly will give you control over him (provided that unit's turn has not ended)
    public void SelectNode()
    {
        //if the unit has already finished its turn
        if (nodeToInterractWith.unitOnNode != null)
        {
            if (!nodeToInterractWith.unitOnNode.GetComponent<Unit>().CanPerformActions())
            {
                DisplayTileInfo();
                DisplayUnitInfo();
                return;
            }
        }
        DeselectNode();

        selectedNode = nodeToInterractWith;

        DisplayTileInfo();

        //right now we allow the selection of both friendlies and enemies
        if (nodeToInterractWith.unitOnNode != null)
        {
            if (selectedNode.unitOnNode.GetComponent<Unit>().HasFInishedMoving)
                SelectUnitNoMovement();
            else
                SelectUnitCanMove();
                mapUI.DisplayPossibleInterractions(interractionManager.GetInterraction(selectedUnit, selectedNode));
        }
        else
        {
            selectedUnitGameObject = null;
            selectedUnit = null;
        }
        selectedNode.Selected = true;
    }

    //selecting a unit that can still move
    public void SelectUnitCanMove()
    {
        selectedUnitGameObject = nodeToInterractWith.unitOnNode;
        selectedUnit = selectedUnitGameObject.GetComponent<Unit>();
        //for the GUI
        DisplayUnitInfo();

        //tiles the unit can move to
        FindSelectableTiles();
        //enemies the unit can shoot
        FindEnemiesInRange();
    }

    //selecting a unit that has moved
    public void SelectUnitNoMovement()
    {
        DeselectNode();

        selectedNode = nodeToInterractWith;
        selectedUnitGameObject = nodeToInterractWith.unitOnNode;
        selectedUnit = selectedUnitGameObject.GetComponent<Unit>();
        DisplayUnitInfo();
        FindEnemiesInRange();
    }

    //determines if the unit can move to the clicked node
    public bool SelectMovementPosition()
    {
        posEffectsManager.GetEffectsOnPosition(clickedNode, selectedNode);
        return clickedNode.Selectable;
    }

    public void MoveUnit()
    {
        //selected node is the one we are moving FROM, not to
        selectedNode.unitOnNode = null;
        PlaceUnit(nodeToInterractWith.posX, nodeToInterractWith.posY, selectedUnitGameObject);


        //!! WE ONLY WANT TO RESET TILE EFFECTS !!                  FIXED ! FIXED 
        //u.ResetEffect();
        //nodes[x, y].ApplyEffects(u);
        selectedUnit.RemoveTileEffects(selectedNode.tileOnNode);
        selectedUnit.ApplyTileEffects(nodeToInterractWith.tileOnNode);

        if (nodeToInterractWith.tileOnNode == terrainTypes[2])
        {
            selectedUnit.mentality.IncreaseEmotion(new EmotionalEffect(Emotion.COURAGE, 10, "Moved in building"));
        }


        selectedUnit.IncreaseFatigue(10);

        selectedUnit.ApplyEmotionalEffect(posEffectsManager.GetEffectsAlreadyCalculated());

        selectedUnit.HasFInishedMoving = true;
    }

    //determening if an enemy is in range
    public bool TargetEnemy()
    {
        return clickedNode.HasEnemyInRange;
    }

    public void AttackUnit()
    {
        Unit n = nodeToInterractWith.unitOnNode.GetComponent<Unit>();
        n.DecreaseHealth(selectedUnit);

        if(n.health.currentValue < 0)
        {
           Corpse c = Die(n);
           nodeToInterractWith.objectsOnNode.Add(c.gameObject);

           selectedUnit.IncreaseEmotion(new EmotionalEffect(Emotion.COURAGE, 30, "Killed enemy"));
        }

        //attacking ends this unit's turn
        selectedUnit.HasFinishedTurn = true;


        DeselectNode();
    }
    Corpse Die(Unit u)
    {
        Debug.Log("Ма той умрял ма");
        Corpse corp = Instantiate(u.CorpsePrefab, u.transform.position, Quaternion.identity).GetComponent<Corpse>();
        corp.GiveIdentity(u);
        Destroy(u.gameObject);
        return corp;
    }


    //set all selection variables to null
    public void DeselectNode()
    {
        //ToggleTileInfo(false);

        selectedUnitGameObject = null;
        selectedUnit = null;
        selectedNode = null;
        //nodeToInterractWith = null;

        mapUI.ClearUI();
        ResetSelectableNodes();
        ResetTargetableEnemies();
        mapUI.HideInterractions();
    }

    public void DeselectInterraction()
    {
        nodeToInterractWith = null;
        clickedNode = null;
    }

    public void Clear()
    {
        DeselectNode();
        DeselectInterraction();
        mapUI.HideInterractions();
    }
    //find the 4 nodes around each node
    void FindNeighbourNodes()
    {
        int c = 0;
        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                //A 4-way connected map
                if (x > 0)
                {
                    nodes[x, y].neighbours.Add(nodes[x - 1, y]);
                    c++;
                }
                if (x < mapSizeX - 1)
                {
                    nodes[x, y].neighbours.Add(nodes[x + 1, y]);
                    c++;
                }
                if (y > 0)
                {
                    nodes[x, y].neighbours.Add(nodes[x, y - 1]);
                    c++;
                }
                if (y < mapSizeY - 1)
                {
                    nodes[x, y].neighbours.Add(nodes[x, y + 1]);
                    c++;

                }
            }
        }
    }

    //finds tiles that the unit can move to
    void FindSelectableTiles()
    {
        Queue<Node> process = new Queue<Node>();

        process.Enqueue(selectedNode);
        selectedNode.visited = true;

        while (process.Count > 0)
        {
            Node n = process.Dequeue();

            selectableNodes.Add(n);
            n.Selectable = true;

            if (n.distance < selectedUnit.MovementPoints.GetValue())
            {
                foreach (Node temp in n.neighbours)
                {
                    if (!temp.visited && temp.unitOnNode == null)
                    {
                        temp.parent = n;
                        temp.visited = true;
                        temp.distance = temp.tileOnNode.movementCost + n.distance;
                        process.Enqueue(temp);

                    }
                }
            }
        }
        foreach (Node n in selectableNodes)
        {
            n.ResetBFS();
        }
    }

    void FindEnemiesInRange()
    {
        Queue<Node> process = new Queue<Node>();

        process.Enqueue(selectedNode);
        selectedNode.visited = true;

        while (process.Count > 0)
        {
            Node n = process.Dequeue();

            if (n.unitOnNode != null)
            {
                if (n.unitOnNode.CompareTag(selectedUnit.enemyTag))
                {
                    enemiesInRange.Add(n);
                    n.HasEnemyInRange = true;
                }
            }
            
            if (n.distance < selectedUnit.Range.GetValue())
            {
                foreach (Node temp in n.neighbours)
                {
                    if (!temp.visited)
                    {
                        temp.parent = n;
                        temp.visited = true;
                        temp.distance = 1 + n.distance;
                        process.Enqueue(temp);
                    }
                }
            }
        }
    }

    public int DistanceBetweenNodes(Node start, Node look)
    {
        int xSum = Mathf.Abs(look.posX - start.posX);
        int ySum = Mathf.Abs(look.posY - start.posY);
        return xSum + ySum;
    }

    //used for the BFS algorithm and displaying 
    //used for tiles the unit can move to and enemies he can shoot
    public void ResetSelectableNodes()
    {
        foreach (Node n in nodes)
        {
            n.Reset();
        }
        selectableNodes.Clear();
    }

    void ResetTargetableEnemies()
    {
        foreach(Node n in nodes)
        {
            n.Reset();
        }
        enemiesInRange.Clear();
    }

    public void DisplayTileInfo()
    {
        mapUI.DisplayTileStats(nodeToInterractWith.tileOnNode);
    }

    public void DisplayUnitInfo()
    {
        mapUI.DisplayUnitStats(nodeToInterractWith.unitOnNode.GetComponent<Unit>());
    }

    public void StartNewPlayerTurn(string playerTag)
    {
        foreach (Unit u in playerTurn.currentPlayer.playerUnits)
        {
            u.HasFinishedTurn = false;
            u.HasFInishedMoving = false;
            u.MentalEffectChance();
        }
        Clear();
    }

    public void EndTurn()
    {
        EndPlayerTurn();
        Debug.Log(playerTurn.currentPlayer.playerTag);
        StartNewPlayerTurn(playerTurn.currentPlayer.playerTag);
        mapUI.SetPlayerTurn(playerTurn.currentPlayer.playerTag);
    }

    public void EndPlayerTurn()
    {
        Debug.Log(playerTurn.currentPlayer.playerUnits.Count);
        foreach (Unit u in playerTurn.currentPlayer.playerUnits)
        {
            u.TurnEnd();
        }
        //you need to change more things here    ! ! !
        if (selectedUnit == null)
            playerTurn.NextPlayer();

        playerTurn.currentPlayer.playerUnits = GetThisPlayerUnits();
    }

    List<Unit> GetThisPlayerUnits()
    {
        List<Unit> temp = new List<Unit>();
        foreach (Node n in nodes)
        {
            if (n.unitOnNode != null)
            {
                if(n.unitOnNode.CompareTag(playerTurn.currentPlayer.playerTag))
                {
                    temp.Add(n.unitOnNode.GetComponent<Unit>());
                }
            }

        }
        return temp;
    }

    public void UnitSleep()
    {
        selectedUnit.Sleep();
        //at terrainTypes[2] we have the building prefab
        if (selectedNode.tileOnNode != terrainTypes[2])
        {
            selectedUnit.IncreaseEmotion(new EmotionalEffect(Emotion.FEAR, 10, "Sleeping outside"));
        }
        if (!posEffectsManager.FriendliesNearby)
            selectedUnit.IncreaseEmotion(new EmotionalEffect(Emotion.FEAR, 10, "Sleeping without allies nearby"));
        Debug.Log("Wat " + posEffectsManager.OneEnemyNearby);
        if (posEffectsManager.OneEnemyNearby)
        {
            selectedUnit.IncreaseEmotion(new EmotionalEffect(Emotion.FEAR, 30, "Sleeping while enemies nearby"));
        }
        Clear();
    }

    public void EnterMeleeState()
    {
        NextTurnState(new TurnMeleeSelected());
    }

    public void MeleeAttack()
    {
        Debug.Log("Melee");
        Unit attacked = nodeToInterractWith.unitOnNode.GetComponent<Unit>();
        Unit winner;
        Unit loser;
        Node loserNode;
        if (selectedUnit.mentality.Courage.GetValue() >= attacked.mentality.Courage.GetValue())
        {
            winner = selectedUnit;
            loser = attacked;
            loserNode = nodeToInterractWith;
        }
        else
        {
            winner = attacked;
            loser = selectedUnit;
            loserNode = selectedNode;
        }

        Corpse c = Die(loser);
        loserNode.objectsOnNode.Add(c.gameObject);
        winner.IncreaseEmotion(new EmotionalEffect(Emotion.COURAGE, 50, "Killed enemy hand-to-hand"));
        winner.IncreaseEmotion(new EmotionalEffect(Emotion.GUILT, 50, "Killed enemy hand-to-hand"));
    }

    public bool CanBeMeleed()
    {
        foreach(Node n in interractionManager.enemiesInRange)
        {
            if(n == nodeToInterractWith)
            {
                return true;
            }
        }
        return false;
    }

    public void ApplyItem()
    {
        Unit u = nodeToInterractWith.unitOnNode.GetComponent<Unit>();
        selectedConsumable.ApplyEffects(u);
       // selectedUnit.inventory.Medkit.ApplyEffects(u);
        selectedConsumable = null;

        Debug.Log(selectedUnit.inventory.Medkit.Amount());
    }

    public void EnterHealState()
    {
        selectedConsumable = selectedUnit.inventory.Medkit;
        selectedInterractionNodes = interractionManager.canHeal;
        NextTurnState(new TurnHealingSelected());
    }

    public void EnterVodkaState()
    {
        selectedConsumable = selectedUnit.inventory.Vodka;
        selectedInterractionNodes = interractionManager.canGiveItem;
        NextTurnState(new TurnHealingSelected());
    }

    public bool CanBeGivenConsumable()
    {
        foreach(Node n in selectedInterractionNodes)
        {
            if(n == nodeToInterractWith)
            {
                return true;
            }
        }
        return false;
    }

    public bool AreTheyAdjacent()
    {
        int i = DistanceBetweenNodes(selectedNode, nodeToInterractWith);
        if (i == 1)
            return true;
        return false;
    }
}