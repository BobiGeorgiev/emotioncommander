﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class MapUI : MonoBehaviour
{
    [SerializeField]
    private Text turnPlayer;

    public Text tileInfo;
    public Text tileDef;
    public Text tileMov;
    public Text tileDesc;
    public Text unitInfo;
    public Text unitHealth;
    public Text unitAttack;
    public Text unitMovement;
    public Text unitRange;
    public Text Fatigue;
    public Text Strain;
    public Text Guilt;
    public Text Courage;
    public Text Fear;

    [SerializeField]
    private Image StateImg;
    [SerializeField]
    private Text StateTitle;
    [SerializeField]
    private Text StateDescription;

    [SerializeField]
    private Button Sleep;
    [SerializeField]
    private Button Heal;
    [SerializeField]
    private Button Melee;
    [SerializeField]
    private Button Vodka;

    [SerializeField]
    private Image InfoPanel;

    [SerializeField]
    Sprite Reckless;
    [SerializeField]
    Sprite Hopeless;
    [SerializeField]
    Sprite Guilty;

    Unit currentUnit;

    void Awake()
    {
        turnPlayer.supportRichText = true;
        tileInfo.supportRichText = true;
        tileDef.supportRichText = true;
        tileMov.supportRichText = true;
        tileDesc.supportRichText = true;
        unitInfo.supportRichText = true;
        unitHealth.supportRichText = true;
        unitAttack.supportRichText = true;
        unitMovement.supportRichText = true;
        unitRange.supportRichText = true;
        Fatigue.supportRichText = true;
        Strain.supportRichText = true;
        Guilt.supportRichText = true;
        Courage.supportRichText = true;
        Fear.supportRichText = true;
        StateTitle.supportRichText = true;
        StateDescription.supportRichText = true;
        StateImg.gameObject.SetActive(false);

        InfoPanel.gameObject.SetActive(false);
        InfoPanel.transform.position = new Vector3(0, 0, 0);
    }

    public void DisplayTileStats(TerrainType t)
    {
        tileInfo.text = t.name;
        tileDef.text = t.defenceBonus.ToString();
        tileMov.text = t.movementCost.ToString();
    }
    public void DisplayUnitStats(Unit u)
    {
        currentUnit = u;

        unitInfo.text = "Name " + u.name;
        unitHealth.text = "Health " + u.health.GetValueString();
        unitAttack.text = "Attack " + u.AttackDamage.GetValue().ToString();
        unitMovement.text = "Movement " + u.MovementPoints.GetValue().ToString();
        unitRange.text = "Range " + u.Range.GetValue().ToString();
        Fatigue.text = "Fatigue " + u.Fatigue.GetValue().ToString();
        Strain.text = "Strain " + u.MentalStrain.GetValue().ToString();
        Guilt.text = "Guilt " + u.mentality.Guilt.GetValue().ToString();
        Courage.text = "Courage " + u.mentality.Courage.GetValue().ToString();
        Fear.text = "Fear " + u.mentality.Fear.GetValue().ToString() + " \n + WWWWW + \n dkdkdkkdkd";

        if (u.mentality.CurrentEmState == null)
            return;
        Debug.Log("MAP UI W");
        StateImg.gameObject.SetActive(true);
        if (u.mentality.CurrentEmState is Recklessness)
            StateImg.sprite = Reckless;
        else if(u.mentality.CurrentEmState is Hopelessness)
            StateImg.sprite = Hopeless;
        else if (u.mentality.CurrentEmState is Guilt)
            StateImg.sprite = Guilty;
        Debug.Log("Wooo " + u.mentality.CurrentEmState.GetTitle());
        StateTitle.gameObject.SetActive(true);
        StateDescription.gameObject.SetActive(true);
        StateTitle.text = u.mentality.CurrentEmState.GetTitle();
        StateDescription.text = u.mentality.CurrentEmState.GetEffectsDescription();
    } 

    public void ClearUI()
    {
        tileInfo.text = "";
        tileDef.text = "";
        tileMov.text = "";
        tileDesc.text = "";
        unitInfo.text = "";
        unitHealth.text = "";
        unitAttack.text = "";
        unitMovement.text = "";
        unitRange.text = "";
        Fatigue.text = "";
        Strain.text = "";
        Guilt.text = "";
        Courage.text = "";
        Fear.text = "";

        StateImg.gameObject.SetActive(false);
        StateTitle.text = "";
        StateDescription.text = "";

        currentUnit = null;
    }

    public void DisplayPossibleInterractions(List<Interraction> actions)
    {
        foreach(Interraction i in actions)
        {
            switch (i)
            {
                case Interraction.SLEEP:
                 //   Sleep.gameObject.SetActive(true);
                    break;
                case Interraction.HEAL:
                    Heal.gameObject.SetActive(true);
                    break;
                case Interraction.MELEE:
                    Melee.gameObject.SetActive(true);
                    break;
                case Interraction.VODKA:
                    Vodka.gameObject.SetActive(true);
                    break;
            }
        }
    }

    public void HideInterractions()
    {
        Sleep.gameObject.SetActive(false);
        Heal.gameObject.SetActive(false);
        Melee.gameObject.SetActive(false);
        Vodka.gameObject.SetActive(false);
    }

    public void DisplayFearCauses()
    {
        if (InfoPanel.IsActive())
            return;
        Text t = InfoPanel.GetComponentInChildren<Text>();
        t.text += "Fear: ";
        foreach (EmotionalEffect m in currentUnit.mentality.GetCurrentMentality())
        {
            if(m.state == Emotion.FEAR)
            {
                t.text += "\n" + m.cause;
            }
        }
        DisplayCauses();
    }

    public void DisplayGuiltCauses()
    {
        if (InfoPanel.IsActive())
            return;
        Text t = InfoPanel.GetComponentInChildren<Text>();
        t.text += "Guilt: ";
        foreach (EmotionalEffect m in currentUnit.mentality.GetCurrentMentality())
        {
            if (m.state == Emotion.GUILT)
            {
                t.text += "\n" + m.cause;
            }
        }
        DisplayCauses();
    }
    public void DisplayCourageCauses()
    {
        if (InfoPanel.IsActive())
            return;
        Text t = InfoPanel.GetComponentInChildren<Text>();
        t.text += "Courage: ";
        foreach (EmotionalEffect m in currentUnit.mentality.GetCurrentMentality())
        {
            if (m.state == Emotion.COURAGE)
            {
                t.text += "\n" + m.cause;
            }
        }
        DisplayCauses();
    }

    void DisplayCauses()
    {
        InfoPanel.gameObject.SetActive(true);

        RectTransform rect = (RectTransform)InfoPanel.transform;
        Vector3 p = new Vector3(InfoPanel.transform.position.x + rect.rect.width*2, 0, 0);
        InfoPanel.transform.position = Input.mousePosition + p;
    }

    public void HideEmotionCauses()
    {
        Text t = InfoPanel.GetComponentInChildren<Text>();
        t.text = "";
        InfoPanel.gameObject.SetActive(false);
        InfoPanel.transform.position = new Vector3(0,0,0);
    }

    public void SetPlayerTurn(string s)
    {
        turnPlayer.text = "";
        turnPlayer.text = s;
    }
}
