﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnState
{
    public TileMap tileMap;
    public TurnState previousState;

    public TurnState()
    {
        previousState = null;
    }

    public void GetInput(ClickInput click)
    {
        switch (click)
        {
            case ClickInput.RightMouseClick:
                BackClick();
                break;
            case ClickInput.SameTileClick:
                SameClick();
                break;
            case ClickInput.EmptyTileClick:
                EmptyClick();
                break;
            case ClickInput.FriendlyClick:
                FriendlyClick();
                break;
            case ClickInput.EnemyClick:
                EnemyClick();
                break;
        }
    }

    protected virtual void BackClick()
    {
        Debug.Log("base back click");
    }
    protected virtual void SameClick()
    {
        Debug.Log("base same click");
    }
    protected virtual void EmptyClick()
    {
        Debug.Log("base empty click");
    }
    protected virtual void FriendlyClick()
    {
        Debug.Log("base friendly click");
    }
    protected virtual void EnemyClick()
    {
        Debug.Log("base enemy click");
    }
}
