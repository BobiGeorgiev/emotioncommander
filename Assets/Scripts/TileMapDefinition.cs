﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TileMapDefinition : ScriptableObject
{
    public TerrainType Default;

    public List<TileEntry> NonDefaulTiles;

    [Serializable] public class TileEntry
    {
        public Vector2Int Position;
        public TerrainType Type;
    }
}
